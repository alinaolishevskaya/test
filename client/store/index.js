import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import state from './state'

// Загружаем модули
const modules = {
    state: state
}

// Загружаем плагины
const plugins = [
    createPersistedState({
        paths: [
            'state',
        ]
    })
]

// Создаем хранилище
Vue.use(Vuex)
const store = new Vuex.Store({
    modules,
    plugins: plugins
})

export default store
