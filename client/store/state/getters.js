export default {
  getData(state) {
    state.states.forEach(item => {
      state.data.sort((a, b) => {
        if (a.state === item && b.state !== item) return 1;
        if (a.state !== item && b.state === item) return -1;
        else return 0
      })
    });


    return state.data
  },

  //получаем общую сводку по всем продуктам
  getSummaryInfo(state) {
    const itemKeys = Object.keys(state.summaryInfo[0]);
    itemKeys.forEach(key => {
      state.data.forEach(item => {
        state.summaryInfo[0][key].count = 0;
      })
    });

    itemKeys.forEach(key => {
      state.data.forEach(item => {
        state.summaryInfo[0][key].count += item[key]
      })
    });

    return state.summaryInfo
  }
}
