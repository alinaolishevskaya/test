export default {
  eventOnUpdateData(state, data) {
    state.data.find((item, idx) => {
      if (item.title === data.title) {
        state.data.splice(idx, 1);
        state.data.push(data);
      }
    });
  }
}
