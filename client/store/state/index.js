import state from './state'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'
import Vue from 'vue'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
