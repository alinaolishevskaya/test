let socket = new WebSocket("ws://127.0.0.1:3000");

export default {
  updateData({state, commit}) {
    socket.onmessage = function(event) {
      commit('eventOnUpdateData', JSON.parse(event.data))
    };
  }
}
