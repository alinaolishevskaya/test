const state = {
  data: [{
    id: 1,
    title: 'Google',
    state: 'purple',
    rating: 5, // рейтинг
    review: 4, // отзывы
    reply: 3, // неотвеченные
    update: 2 // обновления
  },{
    id: 2,
    title: 'Yandex',
    state: 'gray',
    rating: 4,
    review: 3,
    reply: 2,
    update: 1
  },{
    id: 3,
    title: 'Rambler',
    state: 'orange',
    rating: 3,
    review: 2,
    reply: 1,
    update: 0
  },{
    id: 4,
    title: '2gis',
    state: 'gray',
    rating: 2,
    review: 1,
    reply: 0,
    update: 0
  },{
    id: 5,
    title: 'Waze',
    state: 'gray',
    rating: 1,
    review: 0,
    reply: 0,
    update: 0
  }],

  summaryInfo: [{
    review: {
      name: 'отзыв',
      count: 0
    },
    reply: {
      name: 'неотвеченные',
      count: 0
    },
    update: {
      name: 'обновления',
      count: 0
    },
    rating: {
      name: 'рейтинг',
      count: 0
    }
  },
  ],

  states: ['purple', 'orange', 'gray']
};

export const STATE = state;

export default JSON.parse(JSON.stringify(state));
