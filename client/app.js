import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import App from './components/App'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue);

sync(store, router)


 // Axios
import axios from 'axios'
import VueAxios from 'vue-axios'
axios.defaults.baseURL = 'https://enigmatic-ridge-10508.herokuapp.com/'
Vue.use(VueAxios, axios)



const app = new Vue({
  router,
  store,
  ...App
})

export { app, router, store }
